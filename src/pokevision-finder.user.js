// ==UserScript==
// @name        Pokevision Finder
// @namespace   PokevisionFinder
// @include     https://pokevision.com/*
// @version     1
// @grant       none
// ==/UserScript==

window.onload = function () {
  $(".home-sidebar").prepend('<div class="find-pokemon"></div>');
  
  $(".find-pokemon").css("margin-bottom","15px");
  $(".find-pokemon").append('<fieldset>');
  $(".find-pokemon").append('<label for="poketype">Search for a Pokemon:</label><br>');
  $(".find-pokemon").append('<select name="poketype" id="poketype">');
  $(".find-pokemon").append('</select>');
  $(".find-pokemon").append('</fieldset>');
  
  $(".find-pokemon").append('<input type="button" id="reset" value="Reset"><br>');

                
   $("#reset").click(function(){
      location.reload(); 
   });
  
  $("#poketype").change(function(){
      findPokemon($("#poketype").val());
  });
  
  loadPokemon();
}

findPokemon = function(id) {
  $('.leaflet-marker-pane').children('.leaflet-marker-icon-wrapper').each(function () {
    var img = $(this).find(".leaflet-marker-icon").attr('src');
    var last = img.substring(img.lastIndexOf("/") + 1, img.length);
    var final = last.replace(".png", "");
    if(final != id) {
      $(this).remove();
    }
  });
}

loadPokemon = function() {
  $("#poketype").append('<option value="1">Bulbasaur</option>');
  $("#poketype").append('<option value="2">Ivysaur</option>');
  $("#poketype").append('<option value="3">Venasaur</option>');
  $("#poketype").append('<option value="4">Charmander</option>');
  $("#poketype").append('<option value="5">Charmeleon</option>');
  $("#poketype").append('<option value="6">Charizard</option>');
  $("#poketype").append('<option value="7">Squirtle</option>');
  $("#poketype").append('<option value="8">Wartortle</option>');
  $("#poketype").append('<option value="9">Blastoise</option>');
  $("#poketype").append('<option value="10">Caterpie</option>');
  $("#poketype").append('<option value="11">Metapod</option>');
  $("#poketype").append('<option value="12">Butterfree</option>');
  $("#poketype").append('<option value="13">Weedle</option>');
  $("#poketype").append('<option value="14">Kakuna</option>');
  $("#poketype").append('<option value="15">Beedrill</option>');
  $("#poketype").append('<option value="16">Pidgey</option>');
  $("#poketype").append('<option value="17">Pidgeotto</option>');
  $("#poketype").append('<option value="18">Pidgeot</option>');
  $("#poketype").append('<option value="19">Rattata</option>');
  $("#poketype").append('<option value="20">Raticate</option>');
  $("#poketype").append('<option value="21">Spearow</option>');
  $("#poketype").append('<option value="22">Fearow</option>');
  $("#poketype").append('<option value="23">Ekans</option>');
  $("#poketype").append('<option value="24">Arbok</option>');
  $("#poketype").append('<option value="25">Pikachu</option>');
  $("#poketype").append('<option value="26">Raichu</option>');
  $("#poketype").append('<option value="27">Sandshrew</option>');
  $("#poketype").append('<option value="28">Sandslash</option>');
  $("#poketype").append('<option value="29">Nidoran (f)</option>');
  $("#poketype").append('<option value="30">Nidorina</option>');
  $("#poketype").append('<option value="31">Nidoqueen</option>');
  $("#poketype").append('<option value="32">Nidoran (m)</option>');
  $("#poketype").append('<option value="33">Nidorino</option>');
  $("#poketype").append('<option value="34">Nidoking</option>');
  $("#poketype").append('<option value="35">Clefairy</option>');
  $("#poketype").append('<option value="36">Clefable</option>');
  $("#poketype").append('<option value="37">Vulpix</option>');
  $("#poketype").append('<option value="38">Ninetales</option>');
  $("#poketype").append('<option value="39">Jigglypuff</option>');
  $("#poketype").append('<option value="40">Wigglytuff</option>');
  $("#poketype").append('<option value="41">Zubat</option>');
  $("#poketype").append('<option value="42">Golbat</option>');
  $("#poketype").append('<option value="43">Oddish</option>');
  $("#poketype").append('<option value="44">Gloom</option>');
  $("#poketype").append('<option value="45">Vileplume</option>');
  $("#poketype").append('<option value="46">Paras</option>');
  $("#poketype").append('<option value="47">Parasect</option>');
  $("#poketype").append('<option value="48">Venonat</option>');
  $("#poketype").append('<option value="49">Venomoth</option>');
  $("#poketype").append('<option value="50">Diglett</option>');
  $("#poketype").append('<option value="51">Dugtrio</option>');
  $("#poketype").append('<option value="52">Meowth</option>');
  $("#poketype").append('<option value="53">Persian</option>');
  $("#poketype").append('<option value="54">Psyduck</option>');
  $("#poketype").append('<option value="55">Golduck</option>');
  $("#poketype").append('<option value="56">Mankey</option>');
  $("#poketype").append('<option value="57">Primeape</option>');
  $("#poketype").append('<option value="58">Growlithe</option>');
  $("#poketype").append('<option value="59">Arcanine</option>');
  $("#poketype").append('<option value="60">Poliwag</option>');
  $("#poketype").append('<option value="61">Poliwhirl</option>');
  $("#poketype").append('<option value="62">Poliwrath</option>');
  $("#poketype").append('<option value="63">Abra</option>');
  $("#poketype").append('<option value="64">Kadabra</option>');
  $("#poketype").append('<option value="65">Alakazam</option>');
  $("#poketype").append('<option value="66">Machop</option>');
  $("#poketype").append('<option value="67">Machoke</option>');
  $("#poketype").append('<option value="68">Machamp</option>');
  $("#poketype").append('<option value="69">Bellsprout</option>');
  $("#poketype").append('<option value="70">Weepinbell</option>');
  $("#poketype").append('<option value="71">Victreebell</option>');
  $("#poketype").append('<option value="72">Tentacool</option>');
  $("#poketype").append('<option value="73">Tentacruel</option>');
  $("#poketype").append('<option value="74">Geodude</option>');
  $("#poketype").append('<option value="75">Graveler</option>');
  $("#poketype").append('<option value="76">Golem</option>');
  $("#poketype").append('<option value="77">Ponyta</option>');
  $("#poketype").append('<option value="78">Rapidash</option>');
  $("#poketype").append('<option value="79">Slowpoke</option>');
  $("#poketype").append('<option value="80">Slowbro</option>');
  $("#poketype").append('<option value="81">Magnemite</option>');
  $("#poketype").append('<option value="82">Magneton</option>');
  $("#poketype").append('<option value="83">Farfetch\'d</option>');
  $("#poketype").append('<option value="84">Doduo</option>');
  $("#poketype").append('<option value="85">Dodrio</option>');
  $("#poketype").append('<option value="86">Seel</option>');
  $("#poketype").append('<option value="87">Dewgong</option>');
  $("#poketype").append('<option value="88">Grimer</option>');
  $("#poketype").append('<option value="89">Muk</option>');
  $("#poketype").append('<option value="90">Shellder</option>');
  $("#poketype").append('<option value="91">Cloyster </option>');
  $("#poketype").append('<option value="92">Gastly</option>');
  $("#poketype").append('<option value="93">Haunter</option>');
  $("#poketype").append('<option value="94">Gengar</option>');
  $("#poketype").append('<option value="95">Onix</option>');
  $("#poketype").append('<option value="96">Drowzee</option>');
  $("#poketype").append('<option value="97">Hypno</option>');
  $("#poketype").append('<option value="98">Krabby</option>');
  $("#poketype").append('<option value="99">Kingler</option>');
  $("#poketype").append('<option value="100">Voltorb</option>');
  $("#poketype").append('<option value="101">Electrode</option>');
  $("#poketype").append('<option value="102">Exeggcute</option>');
  $("#poketype").append('<option value="103">Exeggutor</option>');
  $("#poketype").append('<option value="104">Cubone</option>');
  $("#poketype").append('<option value="105">Marowak</option>');
  $("#poketype").append('<option value="106">Hitmonlee</option>');
  $("#poketype").append('<option value="107">Hitmonchan</option>');
  $("#poketype").append('<option value="108">Lickitung</option>');
  $("#poketype").append('<option value="109">Koffing</option>');
  $("#poketype").append('<option value="110">Weezing</option>');
  $("#poketype").append('<option value="111">Rhyhorn</option>');
  $("#poketype").append('<option value="112">Rhydon</option>');
  $("#poketype").append('<option value="113">Chansey</option>');
  $("#poketype").append('<option value="114">Tangela</option>');
  $("#poketype").append('<option value="115">Kangaskhan</option>');
  $("#poketype").append('<option value="116">Horsea</option>');
  $("#poketype").append('<option value="117">Seadra</option>');
  $("#poketype").append('<option value="118">Goldeen</option>');
  $("#poketype").append('<option value="119">Seaking</option>');
  $("#poketype").append('<option value="120">Staryu</option>');
  $("#poketype").append('<option value="121">Starmie</option>');
  $("#poketype").append('<option value="122">Mr. Mime</option>');
  $("#poketype").append('<option value="123">Scyther</option>');
  $("#poketype").append('<option value="124">Jynx</option>');
  $("#poketype").append('<option value="125">Electabuzz</option>');
  $("#poketype").append('<option value="126">Magmar</option>');
  $("#poketype").append('<option value="127">Pinsir</option>');
  $("#poketype").append('<option value="128">Tauros</option>');
  $("#poketype").append('<option value="129">Magikarp</option>');
  $("#poketype").append('<option value="130">Gyarados</option>');
  $("#poketype").append('<option value="131">Lapras</option>');
  $("#poketype").append('<option value="132">Ditto</option>');
  $("#poketype").append('<option value="133">Eevee</option>');
  $("#poketype").append('<option value="134">Vaporeon</option>');
  $("#poketype").append('<option value="135">Jolteon</option>');
  $("#poketype").append('<option value="136">Flareon</option>');
  $("#poketype").append('<option value="137">Porygon</option>');
  $("#poketype").append('<option value="138">Omanyte</option>');
  $("#poketype").append('<option value="139">Omastar</option>');
  $("#poketype").append('<option value="140">Kabuto</option>');
  $("#poketype").append('<option value="141">Kabutops</option>');
  $("#poketype").append('<option value="142">Aerodactyl</option>');
  $("#poketype").append('<option value="143">Snorlax</option>');
  $("#poketype").append('<option value="144">Articuno</option>');
  $("#poketype").append('<option value="145">Zapdos</option>');
  $("#poketype").append('<option value="146">Moltres</option>');
  $("#poketype").append('<option value="147">Dratini</option>');
  $("#poketype").append('<option value="148">Dragonair</option>');
  $("#poketype").append('<option value="149">Dragonite</option>');
  $("#poketype").append('<option value="150">Mewtwo</option>');
  $("#poketype").append('<option value="151">Mew</option>');
}